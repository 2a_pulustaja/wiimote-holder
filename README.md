# Wii remote holder #

3D-printable Wii remote holder in order to rig the remote on camera stand etc.

Modelled using [OpenSCAD](http://www.openscad.org/).

## Caveats/Notes/Bugs/etc.:
* The hole on the botton should fit a standard 1/4" 20 nut and bolt, which are commonly used in camera stands. 

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

