rotate([90,0,0]) translate([0,30,-20]) difference() {
    union() {
        difference() {
            translate([0,0,20]) cube([40,60,30], true);
            translate([0,0,25]) cube([36,62,30], true);
        }
        translate([-18,0,34.5]) cube([1,60,1], true);
        translate([18,0,34.5])  cube([1,60,1], true);
        translate([-18,0,34.5]) cube([2,3,1], true);
        translate([18, 0,34.5])  cube([2,3,1], true);
        rotate([90,0,0]) translate([15,5,0]) cylinder(60,5,5,true);
        rotate([90,0,0]) translate([-15,5,0]) cylinder(60,5,5,true);
        translate([0,0,3]) cube([30,60,6], true);
    }
    
    cylinder(25,3.25,3.25, true);
    translate([0,0,6.5]) cube([6.5,11.2,10], true);
    translate([0,0,6.5]) rotate([0,0,60]) cube([6.5,11.2,10], true);
    translate([0,0,6.5]) rotate([0,0,-60]) cube([6.5,11.2,10], true);
}

// cube([11.2,11.2, 20], true);